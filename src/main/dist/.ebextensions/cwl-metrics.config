###############################################################################
## Option Settings
##    Namespace: "aws:elasticbeanstalk:application:environment"
##   OptionName: WebRequestCWLogGroup
##      Default: <EnvironmentName>
##  Description: This is the name of the cloudwatch log group for worker
##
## To get an SNS alert, add a subscription to the Elastic Beanstalk Notification topic.
##    (e.g. set your Notificiation Endpoint to your email address)
##
## Metrics
##  Namespace:  ElasticBeanstalk/<EnvironmentName>
##    Metrics:  Errors, Warnings
##
###############################################################################

Mappings:
  CWLogs:
    ApplicationLog:
      LogFile: "/var/log/exec-1.log"
      TimestampFormat: "%Y-%m-%d %H:%M:%S.%f" # 2016-04-08 10:54:25.048
    FilterPatterns:
      ErrorsMetricFilter: "ERROR"
      WarningsMetricFilter: "WARN"

Outputs:
  ApplicationCWLogGroup:
    Description: "The name of the Cloudwatch Log Group"
    Value: { "Ref" : "AWSEBCloudWatchLogs8832c8d3f1a54c238a40e36f31ef55a0ApplicationLog" }

Resources :
  AWSEBAutoScalingGroup:
    Metadata:
      "AWS::CloudFormation::Init":
        CWLogsAgentConfigSetup:
          files:
            "/tmp/cwlogs/conf.d/exec-1.conf":
              content : |
                [exec-1]
                file = `{"Fn::FindInMap":["CWLogs","ApplicationLog","LogFile"]}`
                log_group_name = `{"Ref":"AWSEBCloudWatchLogs8832c8d3f1a54c238a40e36f31ef55a0ApplicationLog"}`
                log_stream_name = {instance_id}
                datetime_format = `{"Fn::FindInMap":["CWLogs","ApplicationLog","TimestampFormat"]}`
              mode  : "000400"
              owner : root
              group : root

  AWSEBCloudWatchLogs8832c8d3f1a54c238a40e36f31ef55a0ApplicationLog:
    Type: "AWS::Logs::LogGroup"
    DependsOn: AWSEBBeanstalkMetadata
    DeletionPolicy: Retain
    Properties:
      LogGroupName: 
        "Fn::GetOptionSetting":
          Namespace: "aws:elasticbeanstalk:application:environment"
          OptionName: ApplicationCWLogGroup
          DefaultValue: { "Ref": "AWSEBEnvironmentName" }
      RetentionInDays: 7

  AWSEBCWLErrorsMetricFilter:
    Type : "AWS::Logs::MetricFilter"
    Properties :
      LogGroupName : { "Ref" : "AWSEBCloudWatchLogs8832c8d3f1a54c238a40e36f31ef55a0ApplicationLog" }
      FilterPattern : { "Fn::FindInMap": [ "CWLogs", "FilterPatterns", "ErrorsMetricFilter" ] }
      MetricTransformations :
        - MetricValue : 1
          MetricNamespace: { "Fn::Join": [ "/", ["ElasticBeanstalk", { "Ref": "AWSEBEnvironmentName" } ] ] }
          MetricName : Errors

  AWSEBCWLWarningsMetricFilter:
    Type : "AWS::Logs::MetricFilter"
    Properties :
      LogGroupName : { "Ref" : "AWSEBCloudWatchLogs8832c8d3f1a54c238a40e36f31ef55a0ApplicationLog" }
      FilterPattern : { "Fn::FindInMap": [ "CWLogs", "FilterPatterns", "WarningsMetricFilter" ] }
      MetricTransformations :
        - MetricValue : 1
          MetricNamespace: { "Fn::Join": [ "/", ["ElasticBeanstalk", { "Ref": "AWSEBEnvironmentName" } ] ] }
          MetricName : Warnings

##  AWSEBCWLErrorsCountAlarm:
##    Type : "AWS::CloudWatch::Alarm"
##    DependsOn : AWSEBCWLErrorsMetricFilter
##    Properties :
##      AlarmDescription: "Application failing (count too high)."
##      MetricName: Errors
##      Namespace: {"Fn::Join":["/", ["ElasticBeanstalk", {"Ref":"AWSEBEnvironmentName"}]]}
##      Statistic: SampleCount
##      Period: 60
##      EvaluationPeriods: 1
##      Threshold: 10
##      ComparisonOperator: GreaterThanThreshold
##      AlarmActions:
##        - "Fn::If":
##            - SNSTopicExists
##            - "Fn::FindInMap":
##                - AWSEBOptions
##                - options
##                - EBSNSTopicArn
##            - { "Ref" : "AWS::NoValue" }
