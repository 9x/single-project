# Simple Java/Xtend project with Gradle

The build.gradle include:

* Docker Build
* Fat Jar
* AWS Elastic Beanstalk ( Zip with procfile & cloudwatch config )
* Zip/Tar Distribution ( with shell script )
* Maven for release & snapshot
* Eclipse
* Java application executor
* Java Annotation Processing ( command line only ) [aka Xtend Active Annotations] 
